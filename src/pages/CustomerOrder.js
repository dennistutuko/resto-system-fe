import React, { Component } from "react";
import { Container } from "reactstrap";
import { Menu } from "../component";
import { Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";

export default class CustomerOrder extends Component {
  state = {
    menus: [],
    order: [],
    // categoriYangDipilih: 'makanan'
  };

  componentDidMount() {
    fetch("https://resto-api-wave6.herokuapp.com/api/menu")
      .then((response) => response.json())
      .then((result) => {
        this.setState({ menus: result });
      });
  }

  // changeCategory = (value) =>{
  //   this.setState({
  //     categoriYangDipilih: value,
  //     menus: []
  //   })
  // }

  orderanMasuk = (value) => {
    console.log("Menu :", value);
  };

  handleClick(e) {
    e.preventDefault();
  }

  render() {
    return (
      <Container className="App">
        <div className="pb-5 mb-5">
          <h4>
            <strong>Daftar Menu</strong>
          </h4>
          <hr />
          {this.state.menus.map((element) => {
            return (
              <Menu
                namaMenu={element.namaMenu}
                harga={element.harga}
                deskripsi={element.deskripsi}
                key={element.id}
                orderanMasuk={this.orderanMasuk}
              />
            );
          })}
        </div>
        <Link
          to="/customer/order/success"
          className="d-flex justify-content-between align-items-center btn btn-warning fixed-bottom"
        >
          <div className="text-left">
            <p className="font-weight-bold">x items</p>
            <p>x food, x beverage</p>
          </div>
          <div>
            <p className="font-weight-bold">Rp xxx</p>
          </div>
        </Link>
      </Container>
    );
  }
}
