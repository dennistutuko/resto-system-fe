import React, { Component } from "react";
import { withRouter } from "react-router";
import { Container } from "reactstrap";

import "../App.css";

// class Response extends String { json = () => JSON.parse(this) }

async function mockFetch(url, { body }) {
  const { nama, email, noHP } = body;
  // output auth taro sini
  if (nama === "" || email === "" || noHP === "")
    return Promise.reject("Isi tidak boleh kosong!");

  return Promise.resolve({ accessToken: "ini-token" });
}

class CustomerLogin extends Component {
  state = { nama: "", email: "", noHP: "", id: "" };
  set = (name) => (event) => this.setState({ [name]: event.target.value });
  handleSubmit = (event) => {
    event.preventDefault();
    const { nama, email, noHP, id } = this.state;
    const { history } = this.props;
    mockFetch(
      "https://resto-api-wave6.herokuapp.com/api/customer/add-customer",
      {
        body: { nama, email, noHP },
        params: { id },
      }
    )
      // .then(response => console.log(response))
      .then((json) => {
        localStorage.setItem("token", json.accessToken);
        history.push("/customer/order");
      })
      .catch((err) => {
        alert(err);
      });
  };

  render() {
    return (
      <Container className="App">
        <h1>Welcome to Resto!</h1>
        <p>Please fill in form to place your order.</p>
        <form onSubmit={this.handleSubmit}>
          <label>
            name
            <br />
            <input type="text" name="nama" onChange={this.set("nama")} />
          </label>
          <br />
          <label>
            Email
            <br />
            <input type="email" name="email" onChange={this.set("email")} />
          </label>
          <br />
          <label>
            phone
            <br />
            <input type="tel" name="noHP" onChange={this.set("noHP")} />
          </label>
          <br />
          <input type="submit" value="submit" />
        </form>
      </Container>
    );
  }
}

export default withRouter(CustomerLogin);
