import React, { Component } from "react";
import { Container } from "reactstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

import "../App.css";

class RestoHome extends Component {
  state = {
    name: "User Resto",
    id: this.props.match.params.id,
  };

  render() {
    return (
      <Container className="App">
        <h1>Welcome, {this.state.name}</h1>
        <br />
        <Link to="/resto/orders" className="btn btn-primary w-75">
          View Orders
        </Link>
        <br />
        <br />
        <Link to="/resto/menu" className="btn btn-primary w-75">
          View Menu
        </Link>
        <br />
        <br />
        <Link to="/resto/employees" className="btn btn-primary w-75">
          View Employees
        </Link>
      </Container>
    );
  }
}

export default RestoHome;
