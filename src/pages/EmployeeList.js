import React, { Component } from "react";
import { Container } from "reactstrap";

import NameHolderComponent from "../component/NameHolderComponent";
import BottomNav from "../component/BottomNav";

import "../App.css";
import "bootstrap/dist/css/bootstrap.css";

const DATA = [
  {
    id: 1,
    name: "John",
    position: "Owner",
  },
  {
    id: 2,
    name: "Jane",
    position: "Teman",
  },
  {
    id: 3,
    name: "Jables",
    position: "Peliharaan",
  },
];

class EmployeeList extends Component {
  state = {
    restoName: "",
  };

  render() {
    return (
      <Container>
        <h1>{this.state.restoName} Employees</h1>
        <div>
          <NameHolderComponent posts={DATA} />
        </div>
        <BottomNav
          topLink="/resto/employees/add"
          bottomLink="/resto/home"
          topText="Add employee"
          bottomText="Return to main menu"
        />
      </Container>
    );
  }
}

export default EmployeeList;
