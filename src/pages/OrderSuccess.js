import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container } from "reactstrap";

class OrderSuccess extends Component {
  render() {
    return (
      <Container className="App">
        <h1>Order Success</h1>
        <p>Please wait a few minutes, your order is being processed...</p>
        <Link to="../order/list" className="btn btn-primary w-50">
          View Orders
        </Link>
        <br />
        <Link to="../order" className="btn btn-secondary w-50">
          View Menu
        </Link>
      </Container>
    );
  }
}

export default OrderSuccess;
