import React, { Component } from "react";

import OrderDetailHolderComponent from "../component/OrderDetailHolderComponent";
import { Container } from "reactstrap";
import { Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";

const DATA = [
  {
    id: 1,
    name: "Bakso",
    qty: 3,
    price: 10000,
  },
  {
    id: 2,
    name: "Bakwan",
    qty: 1,
    price: 120000,
  },
  {
    id: 3,
    name: "Spageti",
    qty: 2,
    price: 20000,
  },
];

const totalPrice = DATA.map((data) => {
  let arr = data.qty * data.price;

  return arr;
});

let sum = 0;

for (let i = 0; i < totalPrice.length; i++) {
  sum += totalPrice[i];
}

let tax = sum / 10;
let orderTotal = sum + tax;

class YourOrders extends Component {
  handleClick() {
    alert("Order selesai!");
  }

  render() {
    return (
      <Container>
        <h1> Your Orders</h1>
        <OrderDetailHolderComponent posts={DATA} />

        <Container className="fixed-bottom my-4">
          <div className="d-flex justify-content-between">
            <p>Subtotal</p>
            <p>Rp{sum}</p>
          </div>
          <div className="d-flex justify-content-between">
            <p>Tax 10%</p>
            <p>Rp{tax}</p>
          </div>
          <div className="d-flex justify-content-between">
            <p className="font-weight-bold">Order Total</p>
            <p className="font-weight-bold">Rp{orderTotal}</p>
          </div>
          <Link
            to="/customer/order/closed"
            className="btn btn-primary w-100 my-2"
          >
            Pay Now
          </Link>
          <Link
            to="../../customer/order"
            className="btn btn-primary w-100 my-2"
          >
            View Menu
          </Link>
        </Container>
      </Container>
    );
  }
}

export default YourOrders;
