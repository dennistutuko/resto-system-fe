import React, { Component } from 'react';
import { Container } from 'reactstrap'

import OrderHolderComponent from "../component/OrderHolderComponent"
import BottomNav from "../component/BottomNav"

import "../App.css"
import 'bootstrap/dist/css/bootstrap.css';

const DATA = [
    {
        id: 1,
        tableId: 4
    },
    {
        id: 2,
        tableId: 7
    },
    {
        id: 3,
        tableId: 2
    }
]

class OrderList extends Component {


    render() {
        return (
            <Container>
                <Container>
                    <h1>Active Orders</h1>
                    <OrderHolderComponent posts={DATA} />

                </Container>
                <BottomNav topLink="/resto/orders/history" bottomLink="/resto/home" topText="Order history" bottomText="Return to main menu" />
            </Container>

        )
    }
}

export default OrderList