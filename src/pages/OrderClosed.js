import React, { Component } from "react";

import { Container } from "reactstrap";
// import { Link } from 'react-router-dom'

class OrderClosed extends Component {
  render() {
    return (
      <Container className="App">
        <h1>Order Closed</h1>
        <p>Please wait, the waiter will be with you in a few moments...</p>
        <p>Your order total is</p>
        <o>Rp xxx</o>
      </Container>
    );
  }
}

export default OrderClosed;
