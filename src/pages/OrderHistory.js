import React, { Component } from 'react';
import { Container } from 'reactstrap'

import OrderHolderComponent from "../component/OrderHolderComponent"
import { Link } from "react-router-dom"

import "../App.css"
import 'bootstrap/dist/css/bootstrap.css';

const DATA = [
    {
        id: 142,
        tableId: 4
    },
    {
        id: 274,
        tableId: 7
    },
    {
        id: 323,
        tableId: 2
    }
]

class OrderHistory extends Component {


    render() {
        return (
            <Container>
                <h1>Order History</h1>
                <OrderHolderComponent posts={DATA} />

                <Container className="fixed-bottom my-4" >
                    <Link to='/resto/orders' className="btn btn-secondary w-100 my-2" >Return</Link>
                </Container>
            </Container>

        )
    }
}

export default OrderHistory