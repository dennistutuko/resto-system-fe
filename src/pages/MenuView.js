import React, { Component } from "react";
import { Container } from "reactstrap";
import MenuComponent from "../component/MenuComponent";
import BottomNav from "../component/BottomNav";

import "bootstrap/dist/css/bootstrap.css";

const DATA = [
  {
    id: "1",
    name: "bakso",
    price: 10000,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?",
  },
  {
    id: "2",
    name: "jagung",
    price: 12000,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?",
  },
  {
    id: "3",
    name: "kimchi",
    price: 30000,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?",
  },
];

class MenuView extends Component {
  render() {
    return (
      <Container>
        <Container className="App pb-5 mb-5">
          <h1>View Menu</h1>
          <MenuComponent menuList={DATA} />
        </Container>
        <BottomNav
          topLink="/resto/menu/add"
          bottomLink="/resto/home"
          topText="Add new menu"
          bottomText="Return to main menu"
        />
      </Container>
    );
  }
}

export default MenuView;
