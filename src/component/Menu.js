import React from 'react';
import { Col, Card, Button } from 'react-bootstrap'
import { numberWithCommas } from '../utils/koma';

import 'bootstrap/dist/css/bootstrap.css';


const Menu = (props) => {
    return (
        <Col className="mb-4">
            <Card className='shadow w-100'>
                <Card.Body>
                    <Card.Title>{props.namaMenu}</Card.Title>
                    <Card.Text>
                        {props.deskripsi} - Rp.{numberWithCommas(props.harga)}
                    </Card.Text>
                    <Button className="w-50" variant="primary">Pesan</Button>
                </Card.Body>
            </Card>
        </Col>
    )
}
export default Menu

