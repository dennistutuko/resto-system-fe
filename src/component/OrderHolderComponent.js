import React from 'react';
import { Link } from 'react-router-dom'


import 'bootstrap/dist/css/bootstrap.css';
import "../App.css";

export default function OrderHolderComponent(props) {
    const posts = props.posts.map((post) =>
        <Link to={"/resto/orders/" + post.id} key={post.id} className="btn btn-primary d-flex justify-content-between my-2" >
            <p>
                Order #{post.id}
            </p>
            <p>
                Table {post.tableId}
            </p>
        </Link >
    )

    return (
        <div>
            {posts}
        </div>
    )
}