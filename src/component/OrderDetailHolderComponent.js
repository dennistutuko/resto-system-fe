import React from "react";

import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

export default function OrderDetailHolderComponent(props) {
  const posts = props.posts.map((post) => (
    <div
      to={"/resto/orders/" + post.id}
      key={post.id}
      className="d-flex justify-content-between my-2"
    >
      <p>{post.name}</p>
      <div className="d-flex">
        <p className="mx-4">x{post.qty}</p>
        <p>Rp{post.price}</p>
      </div>
    </div>
  ));

  return <div>{posts}</div>;
}
