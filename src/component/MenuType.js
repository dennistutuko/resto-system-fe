import React from 'react'
import { Col, ListGroup } from 'react-bootstrap'


const MenuType = (props) => {
    return (
        <Col>
            <ListGroup>
                <ListGroup.Item>{props.namaType}</ListGroup.Item>
            </ListGroup>
        </Col>
    )
}
export default MenuType