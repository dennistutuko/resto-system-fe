import React from 'react';
import { Link } from 'react-router-dom'
import { Container } from 'reactstrap'

export default function BottomNav(props) {

    return (

        <Container className="fixed-bottom my-4" >
            <Link to={props.topLink} className="btn btn-primary w-100 my-2" >{props.topText}</Link>
            <Link to={props.bottomLink} className="btn btn-secondary w-100 my-2" >{props.bottomText}</Link>
        </Container>
    )
}
