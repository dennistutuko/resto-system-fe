import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import CustomerLogin from "./pages/CustomerLogin";
import CustomerOrder from "./pages/CustomerOrder";
import OrderSuccess from "./pages/OrderSuccess";
import YourOrder from "./pages/YourOrders";
import OrderClosed from "./pages/OrderClosed";

import Home from "./pages/RestoHome";
import LoginResto from "./pages/RestoLogin";
import SignUp from "./pages/RestoSignUp";
import CreateNewResto from "./pages/CreateNewResto";
import NotFound from "./pages/NotFound";
import EmployeeList from "./pages/EmployeeList";
import EmployeeDetails from "./pages/EmployeeDetails";
import EmployeeAdd from "./pages/EmployeeAdd";
import MenuView from "./pages/MenuView";
import MenuDetail from "./pages/MenuDetail";
import MenuAdd from "./pages/MenuAdd";
import OrderHistory from "./pages/OrderHistory";
import OrderList from "./pages/OrderList";
import OrderDetail from "./pages/OrderDetail";

class App extends Component {
  render() {
    return (
      //eslint-disable-next-line
      <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/customer/login" />
          </Route>
          <Route exact path="/customer/login" component={CustomerLogin} />
          <Route exact path="/customer/order" component={CustomerOrder} />
          <Route
            exact
            path="/customer/order/success"
            component={OrderSuccess}
          />
          <Route exact path="/customer/order/list" component={YourOrder} />
          <Route exact path="/customer/order/closed" component={OrderClosed} />

          <Route exact path="/resto">
            <Redirect to="/resto/login" />
          </Route>
          <Route exact path="/resto/home" component={Home} />
          <Route exact path="/resto/home/:id" component={Home} />
          <Route exact path="/resto/login" component={LoginResto} />
          <Route exact path="/resto/SignUp" component={SignUp} />
          <Route exact path="/resto/register" component={CreateNewResto} />
          <Route exact path="/resto/employees" component={EmployeeList} />
          <Route exact path="/resto/employees/add" component={EmployeeAdd} />
          <Route
            exact
            path="/resto/employees/:id"
            component={EmployeeDetails}
          />
          <Route exact path="/resto/menu" component={MenuView} />
          <Route exact path="/resto/menu/add" component={MenuAdd} />
          <Route exact path="/resto/menu/:id" component={MenuDetail} />
          <Route exact path="/resto/orders" component={OrderList} />
          <Route exact path="/resto/orders/history" component={OrderHistory} />
          <Route exact path="/resto/orders/:id" component={OrderDetail} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
